#!/bin/bash

module purge
module load bsc
module load intel/2017.4
module load impi/2017.4
module load netcdf
module load autotools/2020
module load mkl
module load udunits
module load gsl
module load nco
module load ncview

export CC=icc
export CXX=icpc
export FC=ifort
export F77=ifort
export MPIF90=mpiifort
export MPIFORT=mpiifort
export CFLAGS="-mtune=skylake -xCORE-AVX512 -m64"
export FCFLAGS="-mtune=skylake -xCORE-AVX512 -O2"
