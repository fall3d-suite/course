#!/bin/bash
#SBATCH --job-name=FALL3D
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err
#SBATCH --nodes=1
#SBATCH --ntasks=48
#SBATCH --time=00:15:00
#SBATCH --qos=training
#SBATCH --reservation=Computational24

module purge
module load intel/2017.4
module load impi/2017.4
module load netcdf

INPFILE="fuego.inp"
FALLTASK="all"
NX=2
NY=2
NZ=1
NENS=12

if [ "${NENS}" -gt 1 ] ; then
    for i in $(seq ${NENS})
    do
        ENSDIR="$(printf "%04d" ${i})"
        IENS="$(printf "%02d" ${i})"
        echo "Creating folder ${ENSDIR}"
        mkdir -p ${ENSDIR}
        ln -sfr fuego.gefs_p${IENS}.nc ${ENSDIR}/fuego.gefs.nc
    done
fi

srun ./Fall3d.r8.x ${FALLTASK} ${INPFILE} ${NX} ${NY} ${NZ} -nens ${NENS}
