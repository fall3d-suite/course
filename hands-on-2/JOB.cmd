#!/bin/bash
#SBATCH --job-name=FALL3D
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err
#SBATCH --nodes=1
#SBATCH --ntasks=20
#SBATCH --time=00:10:00
#SBATCH --qos=training
#SBATCH --reservation=Computational24

module purge
module load intel/2017.4
module load impi/2017.4
module load netcdf

INPFILE="etna.inp"
FALLTASK="All"
NX=5
NY=2
NZ=2

srun ./Fall3d.r8.x ${FALLTASK} ${INPFILE} ${NX} ${NY} ${NZ}
