import numpy as np
import xarray as xr
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import BoundaryNorm
import cartopy.crs as crs
import cartopy.feature as cfeature
import warnings

warnings.filterwarnings("ignore", category=matplotlib.MatplotlibDeprecationWarning)
plt.rcParams.update({'font.size': 7})

###
### Parameters
###
minval          = 0.1
key             = "tephra_col_mass"
fname           = "../etna.res.nc"
levels          = np.arange(0.0,4,0.25)
vlon, vlat      = 15.0, 37.75
cmap            = plt.cm.RdYlBu_r

###
### Set mininmum level
###
if minval>0: levels[0] = minval

###
### Read file
###
ds = xr.open_dataset(fname)

###
### Generate map
###
proj = crs.PlateCarree()
fig, ax = plt.subplots( subplot_kw={'projection': proj} )

###
### Add map features
###
BORDERS = cfeature.NaturalEarthFeature(
        scale     = '10m',
        category  = 'cultural',
        name      = 'admin_0_countries',
        edgecolor = 'gray',
        facecolor = 'none'
        )
LAND = cfeature.NaturalEarthFeature(
        'physical', 'land', '10m',
        edgecolor = 'none',
        facecolor = 'lightgrey',
        alpha     = 0.8
        )

ax.add_feature(LAND,zorder=0)
ax.add_feature(BORDERS, linewidth=0.4)

###
### Add grid lines
###
gl = ax.gridlines(
	crs         = crs.PlateCarree(),
        draw_labels = True,
	linewidth   = 0.5, 
	color       = 'gray', 
	alpha       = 0.5, 
	linestyle   = '--')
gl.top_labels    = False
gl.right_labels  = False
gl.ylabel_style  = {'rotation': 90}

###
### Add vent location
###
ax.plot(vlon,vlat,color='red',marker='^')

###
### Plot contours
###
cbar = None
for it in range(ds.time.size):
	time_fmt = ds.isel(time=it)['time'].dt.strftime("%d/%m/%Y %H:%M").item()
	ax.set_title(time_fmt, loc='right')
	fc = ax.contourf(
		ds.lon,ds.lat,ds.isel(time=it)[key],
		levels    = levels,
		norm      = BoundaryNorm(levels,cmap.N),
		cmap      = cmap,
		extend    = 'max',
		transform = crs.PlateCarree()
		)

	###
	### Generate colorbar
	###
	if not cbar:
		cbar=fig.colorbar(fc,
			orientation = 'horizontal', 
			label       = r'Tephra column mass [$g~m^{-2}$]',
			)

	###
	### Output plot
	###
	fname_plt = f"map_{it:03d}.png"
	plt.savefig(fname_plt,dpi=300,bbox_inches='tight')

	###
	### Clear contours
	###
	for item in fc.collections: item.remove()
